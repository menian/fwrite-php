<?php

// specify the used file into a variable
$myFile = "mail.txt";

// use fopen to specify the file to be written in, with the proper rights
// http://www.w3schools.com/PHP/php_file.asp explenation of different modes for file handling
$fileHandling = fopen($myFile, 'a+') or die("the file cannot be open");

// get the email from the html trough post and add a comma at the end
$email = $_POST['email'] . ", ";

// who really needs a comment here?!
fwrite($fileHandling, $email);

fclose($fileHandling);

?>

<html>
	<head>
		<title></title>
	</head>
	
	<body>

	<! -- Change action if you want to redirect the user to another page -->
		<form action="" method="post">
			<input type="text" value="Your e-mail" name="email" />
			<input type="submit" value="Submit" />
		</form>

	</body>
</html>